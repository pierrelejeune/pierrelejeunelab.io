const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const webpack = require('webpack');

const ROUTER_BASE = '/';

module.exports = merge(common, {
    mode: 'production',
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                uglifyOptions: {
                    output: {
                        comments: false
                    }
                }
            })
        ]
    },
    output: {
        publicPath: ROUTER_BASE + 'dist/'
    },
    plugins: [
        new webpack.EnvironmentPlugin({
            ROUTER_BASE: ROUTER_BASE,
        })
    ]
});
