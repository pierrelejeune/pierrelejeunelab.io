import {RouteConfig} from "vue-router";

const headerRoutes:Array<RouteConfig> = [
    {
        name: 'header-visual',
        meta: {
            title: 'Visual Header',
            pageTitle: true,
        },
        path: '/header/visual',
        component: () => {
            return import(/* webpackChunkName: 'header' */ "./VisualPage.vue")
        }
    }
];

export default headerRoutes;
