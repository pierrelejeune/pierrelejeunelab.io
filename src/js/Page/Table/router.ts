import {RouteConfig} from "vue-router";

const tableRoutes:Array<RouteConfig> = [
    {
        name: 'table-flex',
        meta: {
            title: 'Table Flex',
            pageTitle: true,
        },
        path: '/table/flex',
        component: () => {
            return import(/* webpackChunkName: 'table' */ "./FlexPage.vue")
        }
    }
];

export default tableRoutes;
