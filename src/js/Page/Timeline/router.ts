import {RouteConfig} from "vue-router";

const timelineRoutes:Array<RouteConfig> = [
    {
        name: 'timeline-vertical',
        meta: {
            title: 'Vertical',
            pageTitle: true,
        },
        path: '/timeline/vertical',
        component: () => {
            return import(/* webpackChunkName: 'table' */ "./VerticalPage.vue")
        }
    }
];

export default timelineRoutes;
