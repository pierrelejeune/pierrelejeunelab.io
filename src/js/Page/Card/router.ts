import {RouteConfig} from "vue-router";

const cardRoutes:Array<RouteConfig> = [
    {
        name: 'card-google',
        meta: {
            title: 'Google Card',
            pageTitle: true,
        },
        path: '/card/google',
        component: () => {
            return import(/* webpackChunkName: 'card' */ "./GooglePage.vue")
        }
    },
    {
        name: 'card-logan',
        meta: {
            title: 'Logan\'s Card',
            pageTitle: true,
        },
        path: '/card/logan',
        component: () => {
            return import(/* webpackChunkName: 'card' */ "./LoganPage.vue")
        }
    },
    {
        name: 'card-visual',
        meta: {
            title: 'Visual Card',
            pageTitle: true,
        },
        path: '/card/visual',
        component: () => {
            return import(/* webpackChunkName: 'card' */ "./VisualPage.vue")
        }
    }
];

export default cardRoutes;
