import $ from 'jquery';

import 'foundation-sites';

import Vue from 'vue';
import App from './App.vue';

import {router} from "./config/router";

require('../scss/app.scss');

const app = new Vue({
    el: '#app',
    props: ['loading','pagename'],
    components: {App},
    template: '<App :pageName="pagename" :loading="loading"></App>',
    router,
});

$(function () {
    $(document).foundation();
    $('div[data-off-canvas] a[href]').on('click', function () {
        $(this).parents("div[data-off-canvas]").foundation('close');
    });
});

router.beforeEach((to, from, next) => {
    if (to.meta.title) {
        document.title = to.meta.title + " | Web Components";
        if (to.meta.pageTitle === true) {
            app.pagename = to.meta.title;
        }else{
            app.pagename = null;
        }
    } else {
        document.title = "Demo Portfolio";
        app.pagename = null;
    }
    app.loading = true;
    window.scroll(0, 0);
    next();
});

router.afterEach((to, from) => {
    app.loading = false;
});
