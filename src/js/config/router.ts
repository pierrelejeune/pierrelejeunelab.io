import Vue from 'vue';
import VueRouter, {RouteConfig} from "vue-router";

import cardRoutes from '../Page/Card/router';
import headerRoutes from '../Page/Header/router';
import tableRoutes from '../Page/Table/router';
import timelineRoutes from "../Page/Timeline/router";

Vue.use(VueRouter);

const baseRoutes: Array<RouteConfig> = [
    {
        name: 'home',
        path: '/',
        redirect: '/card/google'
    },
    {
        name: 'notification-list',
        meta: {
            title: 'Notification',
            pageTitle: true,
        },
        path: '/notification',
        component: () => {
            return import(/* webpackChunkName: 'table' */ "../Page/NotificationPage.vue")
        }
    }
];

const routes: Array<RouteConfig> = baseRoutes.concat(cardRoutes, headerRoutes, tableRoutes, timelineRoutes);

export const router = new VueRouter({
    mode: 'hash',
    base: process.env.ROUTER_BASE,
    linkActiveClass: '',
    linkExactActiveClass: 'active',
    routes: routes
});
