import cardRoutes from '../Page/Card/router';
import headerRoutes from '../Page/Header/router';
import tableRoutes from '../Page/Table/router';
import timelineRoutes from "../Page/Timeline/router";

function createMenu(routes){
    let output:Array<Object> = [];

    routes.forEach(item => {
        output.push({
            label : item.meta.title,
            name : item.name,
        });
    });

    return output;
}

export default [
    {
        label: 'Card',
        children: createMenu(cardRoutes),
    },
    {
        label: 'Header',
        children: createMenu(headerRoutes),
    },
    {
        label: 'Notifications',
        name: 'notification-list'
    },
    {
        label: 'Table',
        children: createMenu(tableRoutes),
    },
    {
        label: 'Timeline',
        children: createMenu(timelineRoutes),
    }
];

