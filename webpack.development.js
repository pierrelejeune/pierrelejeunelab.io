const common = require('./webpack.common.js');
const merge = require('webpack-merge');
const webpack = require('webpack');

const ROUTER_BASE = '/web-components/';

module.exports = merge(common, {
    mode: 'development',
    output:{
        publicPath: ROUTER_BASE + 'dist/'
    },
    plugins: [
        new webpack.EnvironmentPlugin({
            ROUTER_BASE: ROUTER_BASE,
        })
    ]
});
